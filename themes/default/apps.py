from django.apps import AppConfig
from django.db.models.signals import post_migrate


class ThemesDefaultConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "themes.default"
    label = "themes_default"

    def ready(self):
        post_migrate.connect(init_app, sender=self)
        return super().ready()


def init_app(sender, **kwargs):
    """Create initial main navigation menu"""
    from coreplus.navigators.models import Menu, Placeholder

    try:
        main_menu = Menu.objects.get(slug="top-menu")
    except Menu.DoesNotExist:
        main_menu = Menu(
            url="/",
            label="Top Menu",
            slug="top-menu",
        )
        main_menu.save()

    try:
        main_nav = Placeholder.objects.get(slug="top-navigation")
    except Menu.DoesNotExist:
        main_nav = Placeholder(
            url="/",
            label="Top Navigation",
            slug="top-navigation",
            menu_parent=main_menu,
        )
        main_nav.save()
