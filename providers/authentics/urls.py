from allauth.socialaccount.providers.oauth2.urls import default_urlpatterns

from .provider import DecathlonProvider

urlpatterns = default_urlpatterns(DecathlonProvider)
