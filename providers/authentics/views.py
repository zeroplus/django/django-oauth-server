import requests
from allauth.socialaccount.providers.oauth2.views import (
    OAuth2Adapter,
    OAuth2CallbackView,
    OAuth2Client,
    OAuth2Error,
    OAuth2LoginView,
)
from django.conf import settings

from .provider import DecathlonProvider


class DecathlonOauth2Client(OAuth2Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_access_token(self, code):
        from authentics import code_verifier

        data = {
            "redirect_uri": self.callback_url,
            "grant_type": "authorization_code",
            "code": code,
        }
        if self.basic_auth:
            auth = requests.auth.HTTPBasicAuth(self.consumer_key, self.consumer_secret)
        else:
            auth = None
            data.update(
                {
                    "client_id": self.consumer_key,
                    "client_secret": self.consumer_secret,
                    "code_verifier": code_verifier,
                }
            )

        self._strip_empty_keys(data)
        url = self.access_token_url

        resp = requests.post(
            url,
            data=data,
            headers=self.headers,
            auth=auth,
        )
        access_token = None

        if resp.status_code in [200, 201]:
            access_token = resp.json()

        if not access_token or "access_token" not in access_token:
            raise OAuth2Error("Error retrieving access token: %s" % resp.content)
        return access_token


class DecathlonAdapter(OAuth2Adapter):
    client_class = DecathlonOauth2Client
    provider_id = DecathlonProvider.id

    # Fetched programmatically, must be reachable from container
    access_token_url = "{}/o/token/".format(settings.authentic_BASEURL)
    profile_url = "{}/profile/".format(settings.authentic_BASEURL)

    # Accessed by the user browser, must be reachable by the host
    authorize_url = "{}/o/authorize/".format(settings.authentic_BASEURL)

    # NOTE: trailing slashes in URLs are important, don't miss it

    def complete_login(self, request, app, token, **kwargs):
        headers = {"Authorization": "Bearer {0}".format(token.token)}
        resp = requests.get(self.profile_url, headers=headers)
        extra_data = resp.json()
        login = self.get_provider().sociallogin_from_response(request, extra_data)
        return login


class DecathlonLoginView(OAuth2LoginView):
    pass


class DecathlonCallbackView(OAuth2CallbackView):
    pass


oauth2_login = OAuth2LoginView.adapter_view(DecathlonAdapter)
oauth2_callback = DecathlonCallbackView.adapter_view(DecathlonAdapter)
