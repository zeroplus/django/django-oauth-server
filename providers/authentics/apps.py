from django.apps import AppConfig


class DecathlonProviderConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "roviders.authentics"
