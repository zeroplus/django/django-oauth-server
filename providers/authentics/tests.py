import base64
import hashlib
import random
import string
from urllib.parse import parse_qs, urlencode, urlparse

from django.contrib.auth import get_user_model
from django.test import LiveServerTestCase
from oauth2_provider.models import Application

User = get_user_model()

code_verifier = "".join(
    random.choice(string.ascii_uppercase + string.digits)
    for _ in range(random.randint(43, 128))
)
code_verifier = base64.urlsafe_b64encode(code_verifier.encode("utf-8"))
code_challenge = hashlib.sha256(code_verifier).digest()
code_challenge = base64.urlsafe_b64encode(code_challenge).decode("utf-8").rstrip("=")

client_id = "ZWqE7FLFMHw3YWCiUoMTrNz6EOqaa64ltqKpu8Lg"
client_secret = "s1Xe0tJlJUjOM8bDhz7RCkarnO27P71LiRoGYatmBY13D8oXPIv5yBb9gDtFT8bVJIWd6TaNRowZpTZSWQYKvbYFIC6skn9UpdEPBE7TVNFxhLlQ4mCz7maKjFBVDfwN"  # NOQA


class TestCase(LiveServerTestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user(
            username="demo_user", password="demo_password", is_superuser=True
        )
        self.application = Application(
            user=self.user,
            name="superapps",
            client_id=client_id,
            client_type=Application.CLIENT_CONFIDENTIAL,
            redirect_uris="http://127.0.0.1:8000/accounts/authentics/login/callback/",
            skip_authorization=True,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
            client_secret=client_secret,
        )
        self.application.save()
        return super().setUp()

    def test_authentication_and_authorization(self):
        params = {
            "response_type": "code",
            "code_challenge": code_challenge,
            "code_verifier": code_verifier,
            "code_challenge_method": "S256",
            "client_id": self.application.client_id,
            "redirect_uri": self.application.default_redirect_uri,
        }
        path = "/oauth/authorize/"
        url = path + "?" + urlencode(params)
        self.client.force_login(user=self.user)
        resp = self.client.get(url)

        parsed_url = urlparse(resp.url)
        authorization_code = parse_qs(parsed_url.query).get("code")[0]

        # test get authorization token
        payload = {
            "client_id": client_id,
            "client_secret": client_secret,
            "code": authorization_code,
            "code_verifier": code_verifier.decode("utf-8"),
            "redirect_uri": self.application.default_redirect_uri,
            "grant_type": "authorization_code",
        }
        resp = self.client.post("/oauth/token/", data=payload)

        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "access_token")
