import dj_database_url
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *  # NOQA
from .base import env

DEBUG = env("DEBUG")
DOMAIN = env("DOMAIN", "localhost")
ALLOW_WILD_CARD = env("ALLOW_WILD_CARD")
SECRET_KEY = env("SECRET_KEY")
STORAGE_TYPE = env("STORAGE_TYPE", None)


ALLOWED_HOSTS = [
    "www.%s" % DOMAIN,
    DOMAIN,
]

if ALLOW_WILD_CARD:
    ALLOWED_HOSTS += [".%s" % DOMAIN]

if STORAGE_TYPE == "cloudinary":
    CLOUDINARY_CLOUD_NAME = env("CLOUDINARY_CLOUD_NAME")
    CLOUDINARY_API_KEY = env("CLOUDINARY_API_KEY")
    CLOUDINARY_API_SECRET = env("CLOUDINARY_API_SECRET")
    CLOUDINARY_STORAGE = {
        "CLOUD_NAME": CLOUDINARY_CLOUD_NAME,
        "API_KEY": CLOUDINARY_API_KEY,
        "API_SECRET": CLOUDINARY_API_SECRET,
    }
    DEFAULT_FILE_STORAGE = "cloudinary_storage.storage.MediaCloudinaryStorage"
elif STORAGE_TYPE == "minio":
    DEFAULT_FILE_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"
    AWS_ACCESS_KEY_ID = env("AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY = env("AWS_SECRET_ACCESS_KEY")
    AWS_S3_ENDPOINT_URL = env("AWS_S3_ENDPOINT_URL")
    AWS_STORAGE_BUCKET_NAME = env("AWS_STORAGE_BUCKET_NAME")


DATABASES["default"] = dj_database_url.config(conn_max_age=600)  # NOQA


sentry_sdk.init(
    dsn=env("SENTRY_DSN"),
    integrations=[DjangoIntegration()],
    environment="production",
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0,
    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True,
)
