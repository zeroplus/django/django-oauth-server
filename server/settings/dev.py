import os

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *  # NOQA
from .base import env

os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = bool(os.getenv("DEBUG", True))

INTERNAL_IPS = [
    "127.0.0.1",
]

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-%*rvl_4p-+de31n)gn6)80vgfgmy)s+cqh$nzx=^dv$h(kw&p8"

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ["*"]
MIDDLEWARE += [  # NOQA
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

INSTALLED_APPS += [  # NOQA
    "widget_tweaks",
    "django_extensions",
    "debug_toolbar",
]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

GRAPH_MODELS = {
    # "all_applications": True,
    # "group_models": True,
}

# Settup AWS SDK
DEFAULT_FILE_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"

AWS_ACCESS_KEY_ID = env("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = env("AWS_SECRET_ACCESS_KEY")
AWS_S3_ENDPOINT_URL = env("AWS_S3_ENDPOINT_URL")
AWS_STORAGE_BUCKET_NAME = env("AWS_STORAGE_BUCKET_NAME")


sentry_sdk.init(
    dsn=env("SENTRY_DSN"),
    integrations=[DjangoIntegration()],
    environment="development",
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0,
    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True,
)
