import os
from urllib.parse import urlparse

import environ
from django.utils.translation import gettext_lazy as _

API_DESCRIPTION = """# Introduction.

## Welcome to Zero+ Authentication Server

Authentic is centralized user account management. And, this API is documented in **OpenAPI format**
provided by [swagger.io](https://swagger.io/specification/). This API features Cross-Origin
Resource Sharing (CORS) implemented in compliance with [W3C specification](https://www.w3.org/TR/cors/).
And that allows cross-domain communication from the browser. All responses have a wildcard
same-origin which makes them completely public and accessible to everyone, including any code on any site.

## Authentication and Authorization

Authentic SuperApps API offers several forms of authentication:

- BasicAuth
- TokenAuthentication
- Cookie Authentication
- JWT Authentication
- OAuth2
  OAuth2 - an open protocol to allow secure authorization in a simple
  and standard method from web, mobile and desktop applications.
"""

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.dirname(PROJECT_DIR)

##############################################################################
# ENVIRONMENT VARIABLES
# List all enviroment variables used in this project.
# This list should be maintained manually.
##############################################################################

environ.Env.read_env(os.path.join(BASE_DIR, ".env"))

env = environ.Env(
    DEBUG=(bool, False),
    USE_TLS=(bool, False),
    SECRET_KEY=(str, "django-secret-key"),
    SITE_ID=(int, 1),
    SENTRY_DSN=(str, ""),
    SITE_DOMAIN=(str, "127.0.0.1:8000"),
    BASE_URL=(str, "http://127.0.0.1:8000"),
    DEPLOYMENT_ENV=(str, "docker"),
    CORS_ALLOW_ALL_ORIGINS=(bool, 0),
    CORS_ALLOW_CREDENTIALS=(bool, 0),
    CORS_ALLOWED_ORIGINS=(list, []),
    CORS_ALLOWED_ORIGIN_REGEXES=(list, []),
    ALLOW_WILD_CARD=(bool, True),
    STORAGE_TYPE=(str, "whitenoise"),
    POSTGRES_HOST=(str, "127.0.0.1"),
    POSTGRES_PORT=(int, 5432),
    POSTGRES_DB=(str, "authentic_superapps"),
    POSTGRES_USER=(str, "pgadmin"),
    POSTGRES_PASSWORD=(str, "pgadmin_pass"),
    REDIS_PASSWORD=(str, ""),
    REDIS_URL=(str, "redis://127.0.0.1:6379/0"),
    ELASTICSEARCH_HOSTS=(str, "http://127.0.0.1:9200"),
    ELASTICSEARCH_INDEX_NAME=(str, "superapps"),
    GOOGLE_API_KEY=(str, "GOOGLE_API_KEY"),
    GOOGLE_CLIENT_ID=(str, "GOOGLE_CLIENT_ID"),
    GOOGLE_CLIENT_SECRET=(str, "GOOGLE_CLIENT_SECRET"),
    FACEBOOK_API_KEY=(str, "FACEBOOK_API_KEY"),
    FACEBOOK_CLIENT_ID=(str, "FACEBOOK_CLIENT_ID"),
    FACEBOOK_CLIENT_SECRET=(str, "FACEBOOK_CLIENT_SECRET"),
    AUTHENTIC_AUTH_BASEURL=(str, "AUTHENTIC_AUTH_BASEURL"),
    EMAIL_SUBJECT_PREFIX=(str, ""),
    SMTP_SENDER=(str, ""),
    SMTP_PASSWORD=(str, ""),
    SMTP_USERNAME=(str, ""),
    SMTP_PORT=(str, ""),
    SMTP_HOST=(str, ""),
    EMAIL_USE_TLS=(bool, True),
    AWS_ACCESS_KEY_ID=(str, ""),
    AWS_SECRET_ACCESS_KEY=(str, ""),
    AWS_S3_ENDPOINT_URL=(str, ""),
    AWS_STORAGE_BUCKET_NAME=(str, ""),
    CSRF_TRUSTED_ORIGINS=(list, ["http://127.0.0.1:8000"]),
    SEARCH_ENGINE=(str, "whoosh"),
    SEARCH_INDEX_NAME=(str, "django_search"),
    FCM_API_KEY=(str, "firebase_api_key_here"),
    FCM_POST_URL=(str, "https://fcm.googleapis.com/fcm/send"),
    FCM_MAX_RECIPIENTS=(int, 1000),
)

SITE_ID = 1
SITE_NAME = os.getenv("SITE_NAME", "example")
SITE_DOMAIN = env("SITE_DOMAIN")
BASE_URL = env("BASE_URL")
USE_TLS = env("USE_TLS")
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
CSRF_TRUSTED_ORIGINS = env("CSRF_TRUSTED_ORIGINS")

LOCALE_PATHS = [os.path.join(BASE_DIR, "server", "locale")]

INSTALLED_APPS = [
    # apps
    "themes.default",
    "authentics",
    "coreplus.tags",
    "coreplus.settings",
    "coreplus.navigators",
    # REST
    "djoser",
    "rest_framework",
    "drf_spectacular_sidecar",
    "drf_spectacular",
    "corsheaders",
    # other deps
    "mptt",
    "taggit",
    "easy_thumbnails",
    "import_export",
    "notifications",
    "django_filters",
    "push_notifications",
    "phonenumber_field",
    # django
    "django.contrib.gis",
    "django.contrib.admin",
    "django.contrib.admindocs",
    "django.contrib.sites",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.auth",
    # Authentication
    "oauth2_provider",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "allauth.socialaccount.providers.google",
    "allauth.socialaccount.providers.facebook",
    "django_cleanup",
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
]


ROOT_URLCONF = "server.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(PROJECT_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "server.wsgi.application"


# ------------------------------------------------------------------------------
# DATABASE
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases
# ------------------------------------------------------------------------------

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
DATABASES = {
    # "default": {
    #     "ENGINE": "django.db.backends.sqlite3",
    #     "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    # }
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": env("POSTGRES_DB"),
        "USER": env("POSTGRES_USER"),
        "PASSWORD": env("POSTGRES_PASSWORD"),
        "HOST": env("POSTGRES_HOST"),
        "PORT": env("POSTGRES_PORT"),
    }
}

##############################################################################
# AUTHENTICATION
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators
# https://django-allauth.readthedocs.io/en/latest/configuration.html
##############################################################################

if USE_TLS:
    ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"  # or https
ACCOUNT_LOGOUT_ON_PASSWORD_CHANGE = True
ACCOUNT_USERNAME_MIN_LENGTH = 5
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = "mandatory"
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_USERNAME_BLACKLIST = []
ACCOUNT_USERNAME_REQUIRED = False

# ACCOUNT_ADAPTER = "authentics.adapter.AccountAdapter"
ACCOUNT_FORMS = {
    "login": "allauth.account.forms.LoginForm",
    "add_email": "allauth.account.forms.AddEmailForm",
    "change_password": "allauth.account.forms.ChangePasswordForm",
    "set_password": "allauth.account.forms.SetPasswordForm",
    "reset_password": "allauth.account.forms.ResetPasswordForm",
    "reset_password_from_key": "allauth.account.forms.ResetPasswordKeyForm",
    "disconnect": "allauth.socialaccount.forms.DisconnectForm",
    # "signup": "authentics.forms.SignupForm",
}

SIMPLE_JWT = {
    "AUTH_HEADER_TYPES": ("JWT",),
}

AUTH_USER_MODEL = "authentics.User"

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

PASSWORD_HASHERS = [
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.Argon2PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
    "django.contrib.auth.hashers.ScryptPasswordHasher",
]

AUTHENTICATION_BACKENDS = [
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
]

SOCIALACCOUNT_PROVIDERS = {
    "google": {
        # For each OAuth based provider, either add a ``SocialApp``
        # (``socialaccount`` app) containing the required client
        # credentials, or list them here:
        "APP": {
            "client_id": env("GOOGLE_CLIENT_ID"),
            "secret": env("GOOGLE_CLIENT_SECRET"),
            "key": env("GOOGLE_API_KEY"),
        }
    },
    "facebook": {
        # For each OAuth based provider, either add a ``SocialApp``
        # (``socialaccount`` app) containing the required client
        # credentials, or list them here:
        "APP": {
            "client_id": env("FACEBOOK_CLIENT_ID"),
            "secret": env("FACEBOOK_CLIENT_SECRET"),
            "key": env("FACEBOOK_API_KEY"),
        }
    },
    "authentics": {
        # For each OAuth based provider, either add a ``SocialApp``
        # (``socialaccount`` app) containing the required client
        # credentials, or list them here:
        "APP": {
            "client_id": env("AUTHENTIC_CLIENT_ID"),
            "secret": env("AUTHENTIC_CLIENT_SECRET"),
            "key": env("AUTHENTIC_API_KEY"),
        }
    },
}

AUTHENTIC_AUTH_BASEURL = env("AUTHENTIC_AUTH_BASEURL")

##############################################################################
# INTERNATIONALIZATION
# https://docs.djangoproject.com/en/4.0/topics/i18n/
##############################################################################

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGES = [
    ("id", _("Indonesia")),
    ("en", _("English")),
]

##############################################################################
# STATICFILE & STORAGE
##############################################################################

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    # "sass_processor.finders.CssFinder",
]

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

STATICFILES_DIRS = [
    os.path.join(PROJECT_DIR, "static"),
]

STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
STATIC_URL = "/static/"

MEDIA_ROOT = os.path.join(BASE_DIR, "mediafiles")
MEDIA_URL = "/media/"

TAGGIT_CASE_INSENSITIVE = True

######################################################
# EMAIL
######################################################

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

##############################################################################
# SESSION & CACHE
##############################################################################

REDIS_URL = env("REDIS_URL")

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 60 * 60 * 24  # Logout if inactive for 15 minutes
SESSION_SAVE_EVERY_REQUEST = True

if REDIS_URL:
    SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"
    SESSION_CACHE_ALIAS = "default"
    CACHES = {
        "default": {
            "BACKEND": "django_redis.cache.RedisCache",
            "LOCATION": REDIS_URL,
            "OPTIONS": {
                "CLIENT_CLASS": "django_redis.client.DefaultClient",
            },
        },
    }

##############################################################################
# QUEUES
##############################################################################

REDIS_SSL = env("REDIS_SSL", bool, False)
RQ_DATABASE = 1
RQ_URL = urlparse(REDIS_URL)

RQ_QUEUES = {
    "default": {
        "HOST": RQ_URL.hostname,
        "USERNAME": RQ_URL.username,
        "PASSWORD": RQ_URL.password,
        "PORT": RQ_URL.port,
        "DB": RQ_DATABASE,
        "SSL": bool(REDIS_SSL),
        "SSL_CERT_REQS": None,
    },
}

CELERY_BROKER_URL = REDIS_URL
CELERY_CACHE_BACKEND = "default"
CELERY_RESULT_BACKEND = "django-db"

##############################################################################
# LOGGING
##############################################################################

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "WARNING",
    },
    "django.request": {"handlers": ["console"], "level": "ERROR", "propagate": True},
}

##############################################################################
# REST FRAMEWORK
##############################################################################

OAUTH2_PROVIDER = {
    # this is the list of available scopes
    "SCOPES": {
        "read": "Read scope",
        "write": "Write scope",
        "groups": "Access to your groups",
    }
}

DEFAULT_RENDERER_CLASSES = [
    "rest_framework.renderers.JSONRenderer",
    "rest_framework.renderers.BrowsableAPIRenderer",
]

REST_FRAMEWORK = {
    "PAGE_SIZE": 40,
    "DEFAULT_PARSER_CLASSES": (
        "rest_framework.parsers.FormParser",
        "rest_framework.parsers.MultiPartParser",
        "rest_framework.parsers.JSONParser",
    ),
    "DEFAULT_SCHEMA_CLASS": "coreplus.api.schemas.CustomAutoSchema",
    "DEFAULT_VERSIONING_CLASS": "rest_framework.versioning.NamespaceVersioning",
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "DEFAULT_AUTHENTICATION_CLASSES": (
        # "rest_framework.authentication.BasicAuthentication",
        # "rest_framework.authentication.TokenAuthentication",
        # "rest_framework.authentication.SessionAuthentication",
        "rest_framework_simplejwt.authentication.JWTAuthentication",
        "oauth2_provider.contrib.rest_framework.OAuth2Authentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    "DEFAULT_RENDERER_CLASSES": DEFAULT_RENDERER_CLASSES,
    "DEFAULT_FILTER_BACKENDS": [
        "django_filters.rest_framework.DjangoFilterBackend",
    ],
}

DJOSER = {
    # "PASSWORD_RESET_CONFIRM_URL": "#/password/reset/confirm/{uid}/{token}",
    # "USERNAME_RESET_CONFIRM_URL": "#/username/reset/confirm/{uid}/{token}",
    # "ACTIVATION_URL": "#/activate/{uid}/{token}",
    # "SEND_ACTIVATION_EMAIL": True,
    "SERIALIZERS": {
        "activation": "djoser.serializers.ActivationSerializer",
        "password_reset": "djoser.serializers.SendEmailResetSerializer",
        "password_reset_confirm": "djoser.serializers.PasswordResetConfirmSerializer",
        "password_reset_confirm_retype": "djoser.serializers.PasswordResetConfirmRetypeSerializer",
        "set_password": "djoser.serializers.SetPasswordSerializer",
        "set_password_retype": "djoser.serializers.SetPasswordRetypeSerializer",
        "set_username": "djoser.serializers.SetUsernameSerializer",
        "set_username_retype": "djoser.serializers.SetUsernameRetypeSerializer",
        "username_reset": "djoser.serializers.SendEmailResetSerializer",
        "username_reset_confirm": "djoser.serializers.UsernameResetConfirmSerializer",
        "username_reset_confirm_retype": "djoser.serializers.UsernameResetConfirmRetypeSerializer",
        "user_create": "djoser.serializers.UserCreateSerializer",
        "user_create_password_retype": "djoser.serializers.UserCreatePasswordRetypeSerializer",
        "user_delete": "djoser.serializers.UserDeleteSerializer",
        "user": "authentics.api.v1.serializers.UserSerializer",
        "current_user": "authentics.api.v1.serializers.UserSerializer",
        "token": "djoser.serializers.TokenSerializer",
        "token_create": "djoser.serializers.TokenCreateSerializer",
    },
}

##############################################################################
# REST FRAMEWORK DOC
##############################################################################


SPECTACULAR_SETTINGS = {
    "CAMELIZE_NAMES": True,
    "SWAGGER_UI_DIST": "SIDECAR",
    "SWAGGER_UI_FAVICON_HREF": "SIDECAR",
    "REDOC_DIST": "SIDECAR",
    "OAUTH2_FLOWS": ["authorizationCode"],
    "OAUTH2_AUTHORIZATION_URL": None,
    "OAUTH2_TOKEN_URL": None,
    "OAUTH2_REFRESH_URL": None,
    "OAUTH2_SCOPES": None,
    # General schema metadata. Refer to spec for valid inputs
    # https://spec.openapis.org/oas/v3.0.3#openapi-object
    "TITLE": "Authentication Server API",
    "DESCRIPTION": API_DESCRIPTION,
    "TOS": None,
    "CONTACT": {
        "name": "Rizki Sasri Dwitama",
        "url": "https://www.zerocore.co.id",
        "email": "sasri.project@zerocore.com",
    },
    # Optional: MUST contain "name", MAY contain URL
    "LICENSE": {},
    # Statically set schema version. May also be an empty string. When used together with
    # view versioning, will become '0.0.0 (v2)' for 'v2' versioned requests.
    # Set VERSION to None if only the request version should be rendered.
    "VERSION": None,
    "SCHEMA_PATH_PREFIX": r"/api/v[0-9]",
    # Optional list of servers.
    # Each entry MUST contain "url", MAY contain "description", "variables"
    # e.g. [{'url': 'https://example.com/v1', 'description': 'Text'}, ...]
    # "SERVERS": [{'url': 'http:/127.0.0.1/api', 'description': 'Text'}],
    # Tags defined in the global scope
    "TAGS": [
        # {
        #     "name": "user",
        #     "description": "User is awesome",
        # }
    ],
    # Optional: MUST contain 'url', may contain "description"
    "EXTERNAL_DOCS": {},
    # Arbitrary specification extensions attached to the schema's info object.
    # https://swagger.io/specification/#specification-extensions
    "EXTENSIONS_INFO": {
        "x-logo": {
            "url": "/static/authentics/img/logo/logo_api.svg",
            "backgroundColor": "#FFFFFF",
            "altText": "Example logo",
        },
    },
    # Arbitrary specification extensions attached to the schema's root object.
    # https://swagger.io/specification/#specification-extensions
    "EXTENSIONS_ROOT": {
        # "x-tagGroups": [
        #     {
        #         "name": "General",
        #         "tags": ["user", "tag"],
        #     },
        # ],
    },
}
##############################################################################
# PUSH Notification Settings
##############################################################################

WEB_PUSH_SERVER_KEY = "BEFuGfKKEFp-kEBMxAIw7ng8HeH_QwnH5_h55ijKD4FRvgdJU1GVlDo8K5U5ak4cMZdQTUJlkA34llWF0xHya70"  # NOQA

PUSH_NOTIFICATIONS_SETTINGS = {
    "FCM_API_KEY": env("FCM_API_KEY"),
    "FCM_POST_URL": env("FCM_POST_URL"),  # (optional)
    "FCM_MAX_RECIPIENTS": env("FCM_MAX_RECIPIENTS"),
    # "APNS_CERTIFICATE": "/path/to/your/certificate.pem",
    # "APNS_TOPIC": "com.example.push_test",
    # "WP_PRIVATE_KEY": os.path.join(BASE_DIR, "private_key.pem"),
    # "WP_CLAIMS": {"sub": "mailto: sasri.project@gmail.com"},
    # "WP_POST_URL": "" (optional)
}


MAP_WIDGETS = {
    "GOOGLE_MAP_API_SIGNATURE": "",
    "GOOGLE_MAP_API_KEY": "",
    "MAPBOX_API_KEY": "",
}

##############################################################################
# Search Engine Settings
##############################################################################

SEARCH_ENGINE = env("SEARCH_ENGINE")
HAYSTACK_CONNECTIONS = {
    "default": {
        "ENGINE": "haystack.backends.whoosh_backend.WhooshEngine",
        "PATH": os.path.join(BASE_DIR, "whoosh"),
        "STORAGE": "file",
        "POST_LIMIT": 128 * 1024 * 1024,
        "INCLUDE_SPELLING": True,
        "BATCH_SIZE": 100,
        "EXCLUDED_INDEXES": ["thirdpartyapp.search_indexes.BarIndex"],
    },
}

if SEARCH_ENGINE == "elastic_search":
    HAYSTACK_CONNECTIONS = {
        "default": {
            "ENGINE": "haystack.backends.elasticsearch7_backend.Elasticsearch7SearchEngine",
            "URL": env("ELASTICSEARCH_HOSTS"),
            "INDEX_NAME": env("SEARCH_INDEX_NAME"),
            "TIMEOUT": 60 * 5,
            "INCLUDE_SPELLING": True,
            "BATCH_SIZE": 100,
        }
    }
elif SEARCH_ENGINE == "algolia":
    INSTALLED_APPS.append("algoliasearch_django")
    ALGOLIA = {
        "APPLICATION_ID": env("ALGOLIA_APP_ID"),
        "API_KEY": env("ALGOLIA_API_KEY"),
        "INDEX_PREFIX": env("SEARCH_INDEX_NAME"),
        "AUTO_INDEXING": True,
        "RAISE_EXCEPTIONS": env("DEBUG"),
    }
