from django.urls import path
from rest_framework.routers import DefaultRouter

from .viewsets import ContentTypeViewSet, GroupViewSet, PermissionViewSet, UserViewSet

router = DefaultRouter()
router.register("user", UserViewSet, "user")
router.register("group", GroupViewSet, "group")
router.register("contenttype", ContentTypeViewSet, "contenttype")
router.register("permission", PermissionViewSet, "permission")

urlpatterns = []

urlpatterns += router.urls
