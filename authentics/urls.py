from django.conf import settings
from django.urls import include, path
from notifications import urls as notification_urls

from . import views  # NOQA

urlpatterns = [
    path("notifications/", include(notification_urls)),
    path("profile/", views.AccountProfileView.as_view(), name="account_profile"),
]

# Add allauth url if installed
if "allauth" in settings.INSTALLED_APPS:
    from allauth import urls as allauth_url

    urlpatterns += [path("", include(allauth_url))]
