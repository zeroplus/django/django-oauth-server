from datetime import timedelta

from coreplus.tags.models import Tag, TaggedItemBase
from coreplus.utils.avatars import get_gravatar_url
from django.contrib.auth.models import AbstractUser  # NOQA
from django.db import models
from django.utils import timesince, timezone
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from taggit.managers import TaggableManager


class User(AbstractUser):
    uid = models.CharField(
        max_length=255,
        null=True,
        blank=False,
        help_text=_("Unique ID"),
    )
    tags = TaggableManager(
        through="authentics.TaggedUser",
        blank=True,
        related_name="users",
        verbose_name=_("Tags"),
    )

    @property
    def is_partner(self):
        return bool(self.get_partner())

    def get_gravatar_url(self):
        return get_gravatar_url(self.email)

    def get_partner(self):
        return getattr(self, "partner", None)


class TaggedUser(TaggedItemBase):

    content_object = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="tagged_users",
        db_index=True,
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.CASCADE,
        related_name="tagged_users",
        db_index=True,
    )

    class Meta:
        verbose_name = _("Tagged User")
        verbose_name_plural = _("Tagged Users")

    def __str__(self):
        return str(self.tag)


class Profile(models.Model):
    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")

    def __str__(self):
        return self.name


class Deactivation(models.Model):

    user = models.OneToOneField(User, verbose_name=_("user"), on_delete=models.CASCADE)
    started_at = models.DateTimeField(default=timezone.now)
    executed_at = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Deactivation")
        verbose_name_plural = _("Deactivations")

    def __str__(self):
        return self.time_left

    @cached_property
    def deactivated_at(self):
        return self.started_at + timedelta(days=14)

    @cached_property
    def time_left(self):
        return timesince.timeuntil(self.deactivated_at)
